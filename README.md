# IDS 721 Week 3

[![pipeline status](https://gitlab.com/hxia5/ids-721-week-3/badges/main/pipeline.svg)](https://gitlab.com/hxia5/ids-721-week-3/-/commits/main)

## Overview
* This is my repository ofIDS 721 Mini-Project 3 - Create an S3 Bucket using CDK with AWS CodeWhisperer. The project is to create an S3 bucket using AWS Cloud Development Kit (CDK) and AWS CodeWhisperer.
## Purpose
- Create S3 bucket using AWS CDK
- Use CodeWhisperer to generate CDK code
- Add bucket properties like versioning and encryption
- Pass pipeline




## Key Steps

1. Create a new repository in GitLab, must be empty (without README.md file).

2. Sign in to Codecatalyst, go through the set up and create a new enpty project.

3. Under `Code`, in `Dev Environments`, click `Create DEV Environment` and select `AWS Cloud9`.

4. Connect a builder ID to the project, prepare for using Codewhisperer.

5. Go to AWS console, search `IAM`, create a new user, and attach `IAMFullAccess` and `AmazonS3FullAccess` policies to the user.

6. Click on the user, create two inline policies, `CloudFormation` and `Systems Manager`, and give full access.

7. Go to `Security credentials`, create an access key, and save the ID and secret for later.

8. Go to `Cloud9`, run `aws configure` and input the access key and secret, and leave others as blank.

9. Click on the automatically created folder, then click `New Folder` to create a new directory. `cd` to that directory and run `cdk init app --language=typescript`.

10. In this directory, modify the `lib/` file and `bin/` file with the help of `Codewhisperer` to create a new S3 bucket.

11. Run `npm run build` and `cdk synth` to check the code.

12. Run `cdk deploy` to deploy the stack. Here, I failed to deploy the stack and got an error message about Role.

13. To fix the error, create a `zombie.json` file, then run `export ZOMBIE_ROLE=the_role_name_in_error` in the terminal, fill in the role name in error after `=`.

14. Then run `aws iam create-role --role-name=$ZOMBIE_ROLE --assume-role-policy-document file://zombie.json`, put your file path after `file:`.

15. Run `aws iam attach-role-policy --role-name $ZOMBIE_ROLE --policy-arn arn:aws:iam::aws:policy/AdministratorAccess`.

16. Run `cdk deploy` again, and the stack should be deployed successfully.

![Alt text](deploy.png)

17. Use the code in your empty gitlab repository, and push the code to the repository.

18. Go to the AWS console, search `S3`, and check if the bucket is created.

19. Create `.yml` file, go to the GitLab repository, click `CI/CD`, create variables include `AWS_DEFAULT_REGION`, `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `PROD_ACCOUNT_ID`. Then check if the pipeline is passed.

## AWS Codewhisperer
- AWS Codewhisperer is a tool that helps to generate CDK code. It is a web-based tool that can be used to create CDK code for different AWS services. It is a very useful tool for beginners who are not familiar with CDK. In this project, I used Codewhisperer to generate the CDK code for creating an S3 bucket by using following prompts: `// make an S3 bucket`, `// add versioning`, `// add encryption`, and `// add necessary vars`.

## S3 Bucket screenshot

![Alt text](s3.png)

- bucket has versioning enabled

![Alt text](version.png)

- bucket has encryption enabled

![Alt text](encrypt.png)


